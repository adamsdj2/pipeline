#!/bin/bash

echo "---------------------------------------------------------------------"
echo -e "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
echo "Build Number:" $BUILD_NUMBER
echo -e "\n#####################################################################"
FILE_NAME=$(git show --name-only --format="")
FILE_ARRAY=($FILE_NAME)

echo -e "\nCurrent commit contains ${#FILE_ARRAY[@]} element(s):\n"

for i in "${FILE_ARRAY[@]}"; do echo -e "*$i\n"; done

echo "#####################################################################"

RUN=$(basename $j .c)

for j in "${FILE_ARRAY[@]}"
do
	UP_EXT=$(echo $j |awk -F . '{if (NF>1) {printf $NF} fi}')

	if [[ "$UP_EXT" == *cpp* ]]; then
		echo -e "\n$j is a C++ file\ncompiling..."
		if ! g++ $j -lm -o $RUN; then
			echo -e "\nCompilation Failure\n-------------------"
			exit 1
		else
			./$RUN
			echo -e "\nCompilation Success\n-------------------"
		fi
	else
		if [[ "$UP_EXT" == *c* ]]; then
			echo -e "\n$j is a C file\ncompiling..."
			if ! gcc $j -lm -o $RUN; then
				echo -e "\nCompilation Failure\n-------------------"
				exit 1
			else
				./$RUN
				echo -e "\nCompilation Success\n-------------------"
			fi
		fi
	fi
	if [[ "$UP_EXT" == *py* ]]; then
		echo -e "\n$j is a python (.py) file\ncompiling..."
		if ! python3 $j; then
			echo -e "\nCompilation Failure\n--------------------"
			exit 1
		else
			echo -e "\nCompilation Success\n--------------------"
		fi
	fi

	if [[ "$UP_EXT" == *sh* ]]; then
		echo -e "\n$j is a Bash (.sh) file"
	fi

	if [[ "$UP_EXT" == *txt* ]]; then
		echo -e "\n$j is a text (.txt) file"
	fi

	echo -e "\nbuild complete "
	echo -e "\n**********************************************************"
done

echo -e "\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" 
echo "---------------------------------------------------------------------"
