#include <stdio.h>
#include <math.h>

int main(void){
	int x = 576;
	int squareRoot;

	printf("Enter a perfect square: %d\n", x);
	//scanf("%d", &x);
	squareRoot = sqrt(x);
	printf("The square root of your number: %d\n", squareRoot);
	printf("Your number squared: %d\n", x * x);
	return 0;
}
