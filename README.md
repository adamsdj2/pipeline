# Steps I took to start my Continuous Integration Pipeline with GitLab CE and Jenkins Using Ubuntu 20.04 on VirtualBox

## My PC Specifications Relevant to this prototype:
- RAM: 16GB
- Storage: (local disk) 250 GB SSD [Where VirtualBox is stored]
- VirtualBox Snapshot Storage: [2 TB External HDD](https://www.bestbuy.com/site/wd-easystore-2tb-external-usb-3-0-portable-hard-drive-black/6406513.p?skuId=6406513&ref=212&loc=1&extStoreId=166&ref=212&loc=1&gclid=Cj0KCQjw0K-HBhDDARIsAFJ6UGg4xG2rD1I5RE2yl338VvjdF1r5s0hFg8wrHWLoOnL4-sVpSZVsUtsaAliNEALw_wcB&gclsrc=aw.ds "2 TB External HDD")

## Install Oracle VM VirtualBox
[Oracle VM VirtualBox Download](https://www.oracle.com/virtualization/technologies/vm/downloads/virtualbox-downloads.html "Oracle VM VirtualBox Download")

## Setup 2 Virtual Machines with Ubuntu 20.04
    
### Install Ubuntu 20.04 ISO
[Ubuntu 20.04 ISO Download](https://ubuntu.com/download/desktop/thank-you?version=20.04.2.0&architecture=amd64)

[![Ubuntu image](https://drive.google.com/uc?id=1VtnwQJ95C10kihCyg4pvtjS7OxKTwkYJ&export=download)
](https://ubuntu.com/download/desktop/thank-you?version=20.04.2.0&architecture=amd64)

Save this file and remember where it is stored because it will be used later //step x
### GitLab Host VM
#### New VM Setup Requirements:
- RAM: 4 GB
- Storage: 20 GB
- Network: Bridged Adapter 

#### Installing GitLab Community Edition

1. Open the Terminal application
2. Update the system by typing: `sudo apt update`
3. Install SSH server by typing: `sudo apt-get install -y curl openssh-server ca-certificates`
4. (optional) Install PostFix for email notifications by typing: `sudo apt-get install -y postfix`
    - Follow this link for more information: [PostFix Setup](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-postfix-as-a-send-only-smtp-server-on-ubuntu-20-04 "PostFix Setup")
5. Establish Debian Packages for GitLab CE by typing: `curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash`
6. Install GitLab CE by typing: `sudo apt-get install gitlab-ce`
    - (optional) Install GitLab CE with server URL by typing: `sudo EXTERNAL_URL="http://gitlabce.example.com" apt-get install gitlab-ce`
7. Start GitLab CE by typing: `sudo gitlab-ctl reconfigure`
`gitlab-ctl start`

### Jenkins Host VM
### New VM Setup Requirements:
- RAM: Allocate 4GB to VM
- Storage: 20 GB

#### Installing Jenkins
1. Open Terminal Application 
2. Install OpenJDK for Jenkins by typing: `sudo apt install openjdk-11-jdk-headless`
3. Update the system packages by typing: `sudo apt update`
4. Upgrade the system by typing: `sudo apt upgrade`
5. Get the debian packages for Jenkins by typing: `wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -`
6. Get more debian packages for Jenkins (Latest Version) by typing: `sudo sh -c 'echo deb http://pkg.jenkins.io/debian binary/ > /etc/apt/sources.list.d/jenkins.list'`
7. Install Jenkins by typing: `sudo apt install jenkins`
8. Confirm the status of Jenkins after installation by typing: `sudo systemctl status jenkins`
    - If Jenkins is not running type: `sudo systemctl start jenkins`
    - (optional) Enable Jenkins when booting up VM by typing: `sudo systemctl enable --now jenkins`
9. Open your web browser on host or VM to access Jenkins server by typing: `https://LOCALHOST:8080`
    - Your screen should look like this:
    ![Jenkins Image](https://drive.google.com/uc?id=1zWSsLJsHajAVjtfR9A_5RWIc7qBA6YKR&export=download)
10. Unlock Jenkins administrator password by typing in Terminal: `sudo cat /var/lib/jenkins/secrets/initialAdminPassword`
    - Enter password and click continue
11. On next page select "Install Suggested Plugins"
12. Create an First Admin User and select "Save & Finish"

